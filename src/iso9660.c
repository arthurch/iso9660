#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sys/stat.h>

#include <iso9660.h>
#include <rockridge.h>

char *outfile = "sol1.iso";

void hexdump(void *data, size_t sz);

void read_pathtable(iso9660_primary_vd_t *vd, FILE *isof)
{
  size_t pathtable_sz = vd->pathtablesize.lsb;
  uint8_t *ptbuf = (uint8_t *) malloc(pathtable_sz);

  fseek(isof, vd->lba_lpathtable * vd->logicalblocksize.lsb, SEEK_SET);
  fread(ptbuf, 1, pathtable_sz, isof);
  // hexdump(ptbuf, pathtable_sz);

  iso9660_pathtable_entry_t *ptentry = (iso9660_pathtable_entry_t *) ptbuf;

  size_t read_size = 0;
  size_t entrysz;
  unsigned int i = 1;
  while(read_size < pathtable_sz)
  {
    printf("  [%u] \"%.*s\" (parent is %u) : extent location is %u\n", i, ptentry->dirident_length,
      ptentry->dirident, ptentry->parent_index, ptentry->lba_extent);
    entrysz = ISO9660_PT_ENTRY_BASE_SZ + ptentry->dirident_length +
      ((ptentry->dirident_length%2 == 0) ? 0 : 1);
    i++;
    ptentry = (iso9660_pathtable_entry_t *) ((uintptr_t) ptentry + entrysz);
    read_size += entrysz;
  }

  free(ptbuf);
}

void read_rockridge(iso9660_dirrecord_t *record, unsigned int lvl)
{
  size_t skipped_sz = (size_t) ISO9660_DIRRECORD_BASE_SIZE + (size_t) record->ident_length + ((record->ident_length%2 == 0) ? 1 : 0);
  uint8_t *rr = (uint8_t *) ((uintptr_t) record + skipped_sz);
  size_t rr_sz = record->length - skipped_sz;
  printf("%*.sRockridge : (sz skipped=%lu, rrsz=%lu, len=%u)\n", lvl*2+6, " ", skipped_sz, rr_sz, record->ident_length);
  // hexdump(rr, rr_sz);

  if(skipped_sz == record->length) return;

  rockridge_sysentry_t *entry = (rockridge_sysentry_t *) rr;
  size_t read_size = 0;
  size_t entrysz;
  while(read_size < (rr_sz-4))
  {
    if(ROCKRIDGE_MATCH_SIGNATURE(entry->sig, ROCKRIDGE_NM_SIG))
    {
      printf("%*.sNM found : \"%.*s\"\n", lvl*2+6, " ", entry->length - ROCKRIDGE_NM_ENTRY_BASE_SIZE, ((rockridge_NM_entry_t *) entry)->name);
    } else if(ROCKRIDGE_MATCH_SIGNATURE(entry->sig, ROCKRIDGE_PX_SIG))
    {
      rockridge_PX_entry_t *px = (rockridge_PX_entry_t *) entry;
      printf("%*.sPX found : mode=0%o, links=%u, uid=%u, gid=%u, ino=%u\n", lvl*2+6, " ",
        px->filemode.lsb, px->filelinks.lsb, px->uid.lsb, px->gid.lsb, (px->length==44) ? px->ino.lsb : 1234);
    } else
    {
      printf("%*.sEntry found (sig=%.2s, len=%u) : \n", lvl*2+6, " ", entry->sig, entry->length);
    }
    entrysz = entry->length;
    read_size += entrysz;
    entry = (rockridge_sysentry_t *) ((uintptr_t) entry + entrysz);
  }
}

void print_folder(iso9660_primary_vd_t *vd, iso9660_dirrecord_t *dir, unsigned int lvl, FILE *isof)
{
  if((dir->flags & ISO9660_FLAG_DIRECTORY) == 0)
  {
    /* it's not a correct directory */
    return;
  }

  /* load the list of subdirectories and subfiles */
  uint8_t *extent = (uint8_t *) malloc(dir->extent_length.lsb);
  fseek(isof, dir->lba_extent.lsb*vd->logicalblocksize.lsb, SEEK_SET);
  fread(extent, 1, dir->extent_length.lsb, isof);

  // hexdump(extent, 512);

  iso9660_dirrecord_t *dirrecord = (iso9660_dirrecord_t *) extent;
  size_t read_size = 0;
  while(read_size < dir->extent_length.lsb)
  {
    if(dirrecord->length == 0) break;

    if(dirrecord->identifier[0] == 0)
    {
      /* '.' directory */
      printf("%*.s| . (flags:%x, %s, i=%u)\n", lvl*2+4, " ",
        dirrecord->flags, (dirrecord->flags & ISO9660_FLAG_DIRECTORY) ? "folder" : "file", dirrecord->unitsize);
    } else if(dirrecord->identifier[0] == 1)
    {
      /* '..' directory */
      printf("%*.s| .. (flags:%x, %s, i=%u)\n", lvl*2+4, " ",
        dirrecord->flags, (dirrecord->flags & ISO9660_FLAG_DIRECTORY) ? "folder" : "file", dirrecord->unitsize);
    } else
    {
      // /* print date */
      // printf("%*.s| %.*s (flags:%x, %s, ext=%u, sz=%u ko) (creation:", lvl*2+4, " ", dirrecord->ident_length, dirrecord->identifier,
      //   dirrecord->flags, (dirrecord->flags & ISO9660_FLAG_DIRECTORY) ? "folder" : "file", dirrecord->lba_extent.lsb, dirrecord->extent_length.lsb);
      // iso9660_print_date(stdout, &dirrecord->creationdate);
      // printf(")\n");
      /* don't */
      printf("%*.s| %.*s (flags:%x, %s, ext=%u, sz=%u)\n", lvl*2+4, " ", dirrecord->ident_length, dirrecord->identifier,
        dirrecord->flags, (dirrecord->flags & ISO9660_FLAG_DIRECTORY) ? "folder" : "file", dirrecord->lba_extent.lsb, dirrecord->extent_length.lsb);

      // read_rockridge(dirrecord, lvl);
      if(dirrecord->flags & ISO9660_FLAG_DIRECTORY)
      {
        // printf("folder, looking inside... %p %u lvl=%u\n", dirrecord, dirrecord->lba_extent.lsb, lvl+1);
        print_folder(vd, dirrecord, lvl+1, isof);
      }
    }

    read_size += dirrecord->length;
    dirrecord = (iso9660_dirrecord_t *) ((uintptr_t) dirrecord + dirrecord->length);
  }
  free(extent);
}

void print_filesystem_tree(iso9660_primary_vd_t *vd, FILE *isof)
{
  printf("Filesystem tree :\n");
  printf("  (root)\n");

  iso9660_dirrecord_t *root = (iso9660_dirrecord_t *) vd->root_direntry;
  // hexdump(dirrecord, 34);

  print_folder(vd, root, 0, isof);
}

void read_descriptor(iso9660_voldescriptor_t *vd, FILE *isof)
{
  if(vd->type == ISO9660_BOOT_RECORD_VD)
  {
    iso9660_bootrecord_vd_t *bootrecord = (iso9660_bootrecord_vd_t *) vd;
    printf("Boot record descriptor :\n");
    printf("  System ID=%.32s\n", bootrecord->sysid);
    printf("  Boot ID=%.32s\n", bootrecord->bootid);
  } else if(vd->type == ISO9660_PRIMARY_VD)
  {
    iso9660_primary_vd_t *primvd = (iso9660_primary_vd_t *) vd;
    printf("Primary Volume descriptor :\n");
    printf("  System ID=%.32s\n", primvd->sysid);
    printf("  volid=%.32s\n", primvd->volid);
    printf("  volspacesize=%u\n", primvd->volspacesize.lsb);
    printf("  volsetsize=%u\n", primvd->volsetsize.lsb);
    printf("  volsequencenb=%u\n", primvd->volsequencenb.lsb);
    printf("  volsetid=%.32s\n", primvd->volsetid);
    printf("  l path table lba=%u\n", primvd->lba_lpathtable);
    printf("  l opt path table lba=%u\n", primvd->lba_loptpathtable);
    printf("  publisherid=%.32s\n", primvd->publisherid);
    printf("  preparerid=%.32s\n", primvd->preparerid);
    printf("  applicationid=%.32s\n", primvd->applicationid);
    printf("  copyrightid=%.32s\n", primvd->copyrightid);
    printf("  abstractfile=%.32s\n", primvd->abstractfile);
    printf("  bibliofile=%.32s\n", primvd->bibliofile);
    printf("  creation=");
    iso9660_print_date_ascii(stdout, &primvd->volcreation);
    printf("\n");
    printf("  expiration=");
    iso9660_print_date_ascii(stdout, &primvd->volexpiration);
    printf("\n");
    printf("  effective=");
    iso9660_print_date_ascii(stdout, &primvd->voleffective);
    printf("\n");

    printf("Reading the path table :\n");
    read_pathtable(primvd, isof);
    printf("\n");
    print_filesystem_tree(primvd, isof);
  }
}

static char buf[2048];
void read_iso9660(FILE *isof)
{
  size_t seekoff;

  /* skip first 16 sections which are system area */
  fseek(isof, 16*ISO9660_SECTION_SIZE, SEEK_SET);

  /* find every volume descriptors */
  iso9660_voldescriptor_t *voldes;
  fread(buf, 1, ISO9660_VOLUME_DESCRIPTOR_SIZE, isof);
  voldes = (iso9660_voldescriptor_t *) buf;

  printf("Volume descriptors :\n");
  unsigned int i = 0;
  while(voldes->type != ISO9660_VD_SET_TERMINATOR)
  {
    printf("\n[%u] type=%u\n", i, voldes->type);

    seekoff = ftell(isof);
    read_descriptor(voldes, isof);

    fseek(isof, seekoff, SEEK_SET);
    fread(buf, 1, ISO9660_VOLUME_DESCRIPTOR_SIZE, isof);
    voldes = (iso9660_voldescriptor_t *) buf;

    i++;
  }
  printf("\n(last is) [%u] type=%u\n", i, voldes->type);
}

int __main(int argc, char **argv)
{
  printf("opening file %s....\n", outfile);
  FILE *f = fopen(outfile, "rb");
  if(f == NULL)
  {
    printf("[ERROR] Failed to open file\n");
    return -1;
  }

  read_iso9660(f);

  fclose(f);

  return 0;
}

#define LINE_SIZE 16
void hexdump(void *data, size_t sz)
{
  uint8_t *hexdata = (uint8_t *) data;

  char c;
  unsigned int remaining;

  size_t lines = (sz + (LINE_SIZE-1)) / LINE_SIZE;
  unsigned int i, j;
  for(i = 0; i<lines; i++)
  {
    /* line offset */
    printf("%08x", i*LINE_SIZE);
    remaining = ((sz - i*LINE_SIZE) < LINE_SIZE) ? (sz - i*LINE_SIZE) : LINE_SIZE;
    /* line data in hex */
    for(j = 0; j<remaining; j++)
    {
      if(j%8 == 0) printf("  %02x", hexdata[i*LINE_SIZE + j]);
      else printf(" %02x", hexdata[i*LINE_SIZE + j]);
    }
    if(remaining != LINE_SIZE)
    {
      for(j = 0; j<LINE_SIZE-remaining; j++)
      {
        printf("   ");
      }
    }
    /* line data in ascii */
    printf("  |");
    for(j = 0; j<remaining; j++)
    {
      c = (isprint(hexdata[i*LINE_SIZE + j])) ? hexdata[i*LINE_SIZE + j] : '.';
      printf("%c", c);
    }
    printf("|\n");
  }
}
