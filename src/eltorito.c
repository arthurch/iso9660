#include <stdio.h>
#include <stdlib.h>

#include <iso9660.h>
#include <eltorito.h>

const char *eltorito_platforms[3] = {"80x86", "Power PC", "Mac"};
const char *eltorito_mediatypes[5] = {"No Emulation", "1.2 meg diskette", "1.44 meg diskette",
  "2.88 meg diskette", "Hard Disk (drive 80)"};

void hexdump(void *data, size_t sz);

void eltorito_validate(eltorito_validation_entry_t *val)
{
  printf("Validation Entry (sz=%lu) :\n", sizeof(struct eltorito_validation_entry));
  printf("  platformid=%u (%s), volumeid=%.24s, key=%x\n", val->platformid,
    eltorito_platforms[val->platformid], val->volumeid, val->key);

  /* sum all the words to see if it is a valid eltorito boot catalog */
  uint16_t *buf = (uint16_t *) val;
  uint16_t sum;
  unsigned int i;
  for(i = 0; i<sizeof(struct eltorito_validation_entry)/2; i++)
  {
    sum += buf[i];
  }
  if(sum != 0)
  {
    printf("Not a valid Boot Catalog (sum is %x)\n", sum);
  } else
  {
    printf("Valid Boot Catalog\n");
  }
}

void read_boot_record(iso9660_voldescriptor_t *vd, FILE *isof)
{
  if(vd->type != ISO9660_BOOT_RECORD_VD)
  {
    return;
  }
  eltorito_bootrecord_vd_t *bootrecord = (eltorito_bootrecord_vd_t *) vd;

  printf("Boot record descriptor :\n");
  printf("  System ID=%.32s\n", bootrecord->sysid);
  printf("  Boot ID=%.32s\n", bootrecord->bootid);
  printf("  Boot Catalog LBA : %u\n", bootrecord->lba_bootcat);

  uint8_t *bootcat = (uint8_t *) malloc(ISO9660_SECTION_SIZE);
  fseek(isof, bootrecord->lba_bootcat*ISO9660_SECTION_SIZE, SEEK_SET);
  fread(bootcat, 1, ISO9660_SECTION_SIZE, isof);
  hexdump(bootcat, 256);

  eltorito_validation_entry_t *val = (eltorito_validation_entry_t *) bootcat;
  eltorito_validate(val);

  eltorito_section_t *ini = (eltorito_section_t *) (bootcat + sizeof(struct eltorito_validation_entry));
  printf("Initial/Default entry (sz=%lu)\n", sizeof(struct eltorito_section));
  printf("  boot indicator=%x, media type=%u (%s), load segment=%u, system type=%u, sector count=%u, load lba=%u\n",
    ini->bootindicator, ini->mediatype, eltorito_mediatypes[ini->mediatype], ini->loadsegment, ini->systype,
    ini->sectorcount, ini->loadlba);

  eltorito_section_header_t *head = (eltorito_section_header_t *) (bootcat + sizeof(struct eltorito_validation_entry)
    + sizeof(struct eltorito_section));
  printf("Section header entry (sz=%lu)\n", sizeof(struct eltorito_section_header));
  printf("  header indicator=%x, platform id=%x (%s), entries=%u, id=%.28s\n",
    head->headerindicator, head->platformid, eltorito_platformstr(head->platformid), head->entries, head->id);

  unsigned int i;
  eltorito_section_t *se = (eltorito_section_t *) ((uintptr_t) head + sizeof(struct eltorito_section_header));
  for(i = 0; i<head->entries; i++)
  {
    printf("[%u] Section entry (sz=%lu)\n", i, sizeof(struct eltorito_section));
    printf("  boot indicator=%x, media type=%u (%s), load segment=%u, system type=%u, sector count=%u, load lba=%u, selection criteria=%.19s\n",
      se->bootindicator, se->mediatype, eltorito_mediatypes[se->mediatype], se->loadsegment, se->systype,
      se->sectorcount, se->loadlba, se->vendorcriteria);
    se = (eltorito_section_t *) ((uintptr_t) se + sizeof(struct eltorito_section));
  }

  free(bootcat);
}

static char buf[2048];
void read_eltorito(FILE *isof)
{
  size_t seekoff;

  /* skip first 16 sections which are system area */
  fseek(isof, 16*ISO9660_SECTION_SIZE, SEEK_SET);

  /* find every volume descriptors */
  iso9660_voldescriptor_t *voldes;
  fread(buf, 1, ISO9660_VOLUME_DESCRIPTOR_SIZE, isof);
  voldes = (iso9660_voldescriptor_t *) buf;

  printf("Volume descriptors :\n");
  unsigned int i = 0;
  while(voldes->type != ISO9660_VD_SET_TERMINATOR)
  {
    printf("\n[%u] type=%u\n", i, voldes->type);

    seekoff = ftell(isof);
    if(voldes->type == ISO9660_BOOT_RECORD_VD)
    {
      read_boot_record(voldes, isof);
    }

    fseek(isof, seekoff, SEEK_SET);
    fread(buf, 1, ISO9660_VOLUME_DESCRIPTOR_SIZE, isof);
    voldes = (iso9660_voldescriptor_t *) buf;

    i++;
  }
  printf("\n(last is) [%u] type=%u\n", i, voldes->type);
}
static const char *debianiso = "/home/arthur/Documents/iso/debian-live-10.3.0-amd64-xfce.iso";
static const char *isofile = "sol1.iso";
int _main(int argc, char **argv)
{
  printf("opening file %s....\n", debianiso);
  FILE *f = fopen(isofile, "rb");
  if(f == NULL)
  {
    printf("[ERROR] Failed to open file\n");
    return -1;
  }

  read_eltorito(f);

  fclose(f);

  return 0;
}
