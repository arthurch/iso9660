CC=gcc

CFLAGS:=-g -Wall -Iinclude/
LFLAGS:=-g

SRCS:=$(wildcard src/*.c)
OBJS:=$(SRCS:.c=.o)
HEADERS:=$(wildcard include/*.h)

OUTPUT=testiso

.PHONY: all clean

all: $(OUTPUT)

$(OUTPUT): $(OBJS)
	@rm -f testiso
	$(CC) $(LFLAGS) -o $@ $(OBJS)
	@rm -f *.o
	@rm -f *.d

%.o: %.c
	$(CC) -MD $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(OUTPUT)
	rm -f *.o
	rm -f *.d

-include $(SRCS:.c=.d);
