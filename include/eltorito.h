#ifndef ELTORITO_H
#define ELTORITO_H 1

#include <iso9660.h>

#define ELTORITO_BOOTRECORD_SECTION 0x11
#define ELTORITO_SECTOR_SIZE 512

struct eltorito_bootrecord_vd {
  /* likewise struct iso9660_voldescriptor */
  uint8_t type;
  uint8_t identifier[5];
  uint8_t version;
  /* boot record specific values */
  uint8_t sysid[32]; /* id of the system acting on our boot system */
  uint8_t bootid[32]; /* id of the boot system (unused, set to 0) */
  /* El-Torito spectific values */
  uint32_t lba_bootcat; /* LBA address of the El-Torito Boot Catalog */
  uint8_t data[1973]; /* unused, set to 0 */
} __attribute__((packed));
typedef struct eltorito_bootrecord_vd eltorito_bootrecord_vd_t;

#define ELTORITO_PLATFORM_x86 0
#define ELTORITO_PLATFORM_POWERPC 1
#define ELTORITO_PLATFORM_MAC 2

struct eltorito_validation_entry {
  uint8_t headerid; /* Must be 1 */
  uint8_t platformid; /* Platform ID :
    0 = 80x86
    1 = Power PC
    2 = Mac */
  uint16_t reserved; /* reserved, should be 0 */
  uint8_t volumeid[24]; /* Identifier of the manufacturer/developer of the volume */
  uint16_t checksum; /* sum of all the words in this record */
  uint16_t key; /* key byte, must be 0x55aa */
};
typedef struct eltorito_validation_entry eltorito_validation_entry_t;

struct eltorito_section_header {
  uint8_t headerindicator; /* Header indicator :
    90 = Header, more to follow
    91 = Final header */
  uint8_t platformid; /* Platform ID :
    0 = 80x86
    1 = Power PC
    2 = Mac */
  uint16_t entries; /* Number of section entries following */
  uint8_t id[28]; /* ID string identifying a section */
};
typedef struct eltorito_section_header eltorito_section_header_t;

struct eltorito_section {
  uint8_t bootindicator; /* Boot indicator :
    88 = bootable, 00 = not bootable */
  uint8_t mediatype; /* Boot media type :
    0 = No Emulation
    1 = 1.2 meg diskette
    2 = 1.44 meg diskette
    3 = 2.88 meg diskette
    4 = Hard Disk (drive 80) */
  uint16_t loadsegment; /* Load segment for the initial
    boot image (if set to 0 then system should use the traditional
    segment 0x7c0) */
  uint8_t systype; /* System type */
  uint8_t unused0; /* reserved, should be 0 */
  uint16_t sectorcount; /* Number of virtual/emulated sectors the system
    will store at load segment during the initial boot procedure */
  uint32_t loadlba; /* start address of the virtual disk using LBA addressing */
  uint8_t selectioncriteriatype; /* should be 0 */
  uint8_t vendorcriteria[19]; /* supposed to be vendor's selection criteria */
};
typedef struct eltorito_section eltorito_section_t;

extern const char *eltorito_platforms[3];
extern const char *eltorito_mediatypes[5];

static inline const char *eltorito_platformstr(uint8_t platformid)
{
  return (platformid < 3) ? eltorito_platforms[platformid] : ((platformid == 0xef) ? "EFI" : "[unknown]");
}

#endif
