#ifndef ROCKRIDGE_H
#define ROCKRIDGE_H 1

/* Rockridge is an extension of the iso9660 filesystem to allow POSIX-like
  informations about stored files. It works by adding a list of entries ("System
  Entries") in the system use area at the end of each directory record.
  Each entry is at least 4 bytes long and has the following structure :
      Bytes      |    Description
      0 - 1      |  2 bytes string representing the signature of the entry
        2        |  The length in bytes of the entry
        3        |  Version of the entry type (typically 1)
    4 - (length) |  Data used defined by the type of entry it is
*/

#include <stdint.h> /* used for precisely sized variables. Alternatively,
  you could just define uint32_t, uint16_t, and uint8_t being respectively
  32 bits, 16 bits and 8 bits long */

#include "iso9660.h"

/* other entries are not defined here : PN (device number), SL (symbolic
    link), CL (child link), PL (parent link), RE (relocated directory), TF
    (time stamps), SF (file data in sparse file format) */
#define ROCKRIDGE_PX_SIG "PX"
#define ROCKRIDGE_NM_SIG "NM"

#define ROCKRIDGE_MATCH_SIGNATURE(SIG, MATCH) \
  (((SIG)[0] == (MATCH)[0]) && ((SIG)[1] == (MATCH)[1]) ? 1 : 0)

struct rockridge_sysentry {
  uint8_t sig[2]; /* two bytes representing an identification of the entry */
  uint8_t length; /* length in bytes of the entry */
  uint8_t version; /* version of this specific entry type (typically 1) */
  uint8_t data[]; /* data of arbitrary size depending on the type of entry */
} __attribute__((packed));
typedef struct rockridge_sysentry rockridge_sysentry_t;

/* The PX entry represents POSIX files attributes and user/group
  permissions. It should be mandatory according to the Rockridge
  specification (but you know, specifications...).
  Note that each field are supposed to represents an information
  about POSIX files (as in the stat structure in linux for example) */
struct rockridge_PX_entry {
  /* same as the rockridge_sysentry structure */
  uint8_t sig[2];
  uint8_t length; /* should be 44 if ino is present, otherwise 36 */
  uint8_t version;
  /* PX specific fields */
  uint32_lsb_msb_t filemode; /* used for the st_mode field (representing file
    type and modes). Possible values should be found in sys/stat.h. */
  uint32_lsb_msb_t filelinks; /* used for the st_nlink field (representing the
    number of hard links to this file) */
  uint32_lsb_msb_t uid; /* used for the st_uid field (representing the User ID
    of the owner) */
  uint32_lsb_msb_t gid;  /* used for the st_gid field (representing the Group ID
    of the owner) */
  uint32_lsb_msb_t ino; /* file serial number, used for the st_ino field
    (Note that this last field is often not present at all so be careful
    using it) */
} __attribute__((packed));
typedef struct rockridge_PX_entry rockridge_PX_entry_t;

#define ROCKRIDGE_NM_ENTRY_BASE_SIZE 5
/* The NM entry allows to store the content of an alternate name to support
  POSIX-style or other names. (This entry is optional). If more that one NM
  entry are present for a same directory record, then their name filed should
  be concatenated together, until a CONTINUE flag with value zero is encountered.
  If the directory record file identifier is 0 (indicating a '.' directory), then
  the CURRENT bit of the flag should be set to 1. If it is 1 (indicating a '..'
  directory), then the PARENT bit of the flag should be set to 1. */
struct rockridge_NM_entry {
  /* same as the rockridge_sysentry structure */
  uint8_t sig[2];
  uint8_t length; /* may vary depending on the length of name field */
  uint8_t version;
  /* NM specific fields */
  uint8_t flags; /* flags for the NM entry :
  bit 0 - CONTINUE flag : alternate name continues in next NM entry if set
  bit 1 - CURRENT flag : if set, the alternate name refers to the current
    directory ('.')
  bit 2 - PARENT flag : if set, the alternate name refers to the parent
    directory ('..')
  bits 3 - 7 : RESERVED (should be set to 0) */
  uint8_t name[]; /* actual alternative name */
} __attribute__((packed));
typedef struct rockridge_NM_entry rockridge_NM_entry_t;

#endif
