#ifndef ISO9660_H
#define ISO9660_H 1

#include <stdint.h> /* used for precisely sized variables. Alternatively,
  you could just define uint32_t, uint16_t, and uint8_t being respectively
  32 bits, 16 bits and 8 bits long */

/* structure of an iso9660 filesystem :
  (each section is usually 2048 B large, but in theory it could be anything)
  sections  |  description
    0 - 16  |  System area (arbitrary data, usually boot informations)
   16 - ... |  Data area, used to describe iso9660 filesystem and to
                store file content

  The data area begins with a set of Volume Descriptors, describing
    the content of the filesystem :
  Volume descriptor 1
  Volume descriptor 2
  ...
  Volume descriptor set terminator
  (note that each volume descriptor is 2048 B wide)

  Each volume descritor is of one of the following type :
    0 : Boot Record (booting informations)
    1 : Primary Volume Descriptor (informations about the filesystem, and
      about the volume)
    2 : Supplementary Volume Descriptor (additionnal informations about the
      volume)
    3 : Volume Partition Descriptor (informations about potential partitions)
    255 : Volume Descriptor Set Terminator (indicates the end of the set)

  Two ways of accessing the files tree :
  - Via the path table which lists every folders (accessed by its LBA
      address, stored in primary volume descriptor)
  - Via the directory records structure (by recursing from root
      directory, its record stored in primary volume descriptor)

  Note on System area (first 32 KiB) : it usually contains informations
    not related with iso9660 filesystem. Typically it could contain a Master
    Boot Record, a GUID Partition Table or/and even a Apple Partition Map. This
    would allow the iso filesystem to be stored on an hard-disk-drive like device
    (USB stick, ...).

  Note on LBA addresses : Logical Block Addressing, the linear address
    of the section.
    Formula : Address (bytes) = LBA Address * ISO9660_SECTION_SIZE

  Note on packed attributes : every structures are packed because they
    are supposed to map exactly the content of an iso9660 disk or file,
    we don't want gcc to add any padding inside them
*/

#define ISO9660_SYSTEM_AREA 16

#define ISO9660_SECTION_SIZE 2048
#define ISO9660_VOLUME_DESCRIPTOR_SIZE ISO9660_SECTION_SIZE

/* Types of volume descriptors */
#define ISO9660_BOOT_RECORD_VD 0
#define ISO9660_PRIMARY_VD 1
#define ISO9660_SUPPLEMENTARY_VD 2
#define ISO9660_VOLUME_PARTITION_DESCRIPTOR 3
#define ISO9660_VD_SET_TERMINATOR 255

struct uint32_lsb_msb {
  uint32_t lsb;
  uint32_t msb;
} __attribute__((packed));
typedef struct uint32_lsb_msb uint32_lsb_msb_t;

struct uint16_lsb_msb {
  uint16_t lsb;
  uint16_t msb;
} __attribute__((packed));
typedef struct uint16_lsb_msb uint16_lsb_msb_t;

/* Date and time used in volume descriptors */
struct iso9660_datetime_ascii {
  /* These fields are in ASCII digits */
  uint8_t year[4]; /* from 1 to 9999 */
  uint8_t month[2]; /* from 1 to 12 */
  uint8_t day[2]; /* from 1 to 31 */
  uint8_t hours[2]; /* from 0 to 59 */
  uint8_t minutes[2]; /* from 0 to 59 */
  uint8_t seconds[2]; /* from 0 to 59 */
  uint8_t hundredths[2]; /* hundredths of a second from 0 to 99 */
  /* except for the GMT offset which represents
    the time zone offset from GMT in 15 minutes intervals
    starting at -48 (west) to 52 (east).
    Ex: value 0 = GMT-12hours (-48 * 15minutes), paris is GMT+1h = value 52,
    GMT+0h = value 48 */
  uint8_t gmt_offset; /* note that some softwares always set this field to 0
    regardless of the GMT offset of the represented date and time (in that case
    it's often a GMT+0 time) */
} __attribute__((packed));
typedef struct iso9660_datetime_ascii iso9660_datetime_ascii_t;

/* Date and time used in directory records (used to save disk space) */
struct iso9660_datetime {
  /* These fields are real numbers */
  uint8_t year; /* number of years since 1900 */
  uint8_t month; /* from 1 to 12 */
  uint8_t day; /* from 1 to 31 */
  uint8_t hours; /* from 0 to 59 */
  uint8_t minutes; /* from 0 to 59 */
  uint8_t seconds; /* from 0 to 59 */
  /* the GMT offset represents the time zone offset from GMT in
    15 minutes intervals starting at -48 (west) to 52 (east).
    Ex: value 0 = GMT-12hours (-48 * 15minutes), paris is GMT+1h = value 52,
    GMT+0h = value 48 */
  uint8_t gmt_offset; /* note that some softwares always set this field to 0
    regardless of the GMT offset of the represented date and time (in that case
    it's often a GMT+0 time) */
} __attribute__((packed));
typedef struct iso9660_datetime iso9660_datetime_t;

/*** volume descriptor set ***/

struct iso9660_voldescriptor {
  /* Volume descriptor type code :
    0 is boot record
    1 is primary volume descriptor
    2 is supplementary volume descriptor
    3 is volume partition descriptor
    255 is volume descriptor set terminator */
  uint8_t type;
  uint8_t identifier[5]; /* always "CD001" */
  uint8_t version; /* always 0x01 */

  /* this field depends on the type of Volume Descriptor (and may be unused
    if it is a volume descriptor set terminator) */
  uint8_t data[2041];
} __attribute__((packed));
typedef struct iso9660_voldescriptor iso9660_voldescriptor_t;

struct iso9660_bootrecord_vd {
  /* likewise struct iso9660_voldescriptor */
  uint8_t type;
  uint8_t identifier[5];
  uint8_t version;
  /* boot record specific values */
  uint8_t sysid[32]; /* id of the system acting on our boot system */
  uint8_t bootid[32]; /* id of the boot system */
  uint8_t data[1977]; /* custom data used by the boot system */
} __attribute__((packed));
typedef struct iso9660_bootrecord_vd iso9660_bootrecord_vd_t;

struct iso9660_primary_vd {
  /* likewise struct iso9660_voldescriptor */
  uint8_t type;
  uint8_t identifier[5];
  uint8_t version;
  /* primary volume descriptor specific values */
  uint8_t unused0; /* set to 0 */
  uint8_t sysid[32]; /* name of the system acting on sectors 0x00-0x0f
    for the volume */
  uint8_t volid[32]; /* id of this volume */
  uint8_t unused1[8]; /* set to 0 */
  uint32_lsb_msb_t volspacesize; /* number of logical blocks of this volume */
  uint8_t unused2[32]; /* set to 0 */
  uint16_lsb_msb_t volsetsize; /* size of the set (number of disks) (if there is
    more than one disk) */
  uint16_lsb_msb_t volsequencenb; /* number of this disk in the volume set */
  uint16_lsb_msb_t logicalblocksize; /* size in bytes of a logical block */
  uint32_lsb_msb_t pathtablesize; /* size in bytes of the path table */
  uint32_t lba_lpathtable; /* LBA address of the little-endian path table */
  uint32_t lba_loptpathtable; /* LBA address of an optional little-endian path table (0 means
    no optional path table exists) */
  uint32_t lba_mpathtable; /* LBA address of the big-endian path table */
  uint32_t lba_moptpathtable; /* LBA address of an optional big-endian path table (0 means
    no optional path table exists) */
  uint8_t root_direntry[34]; /* directory entry for the root directory */
  uint8_t volsetid[128]; /* identifier of the volume set of which this volume is a member */
  /* for all following identifiers, for extended information, the first byte should be 0x5f
    followed by the filename of a file in the root directory, containing extra informations.
    If an identifier is not specified, all bytes should be 0x20 */
  uint8_t publisherid[128]; /* volume publisher */
  uint8_t preparerid[128]; /* person(s) who prepared the data */
  uint8_t applicationid[128]; /* how the data are recorded */
  /* the three following filenames are supposed to be found in the root directory and
    should contains only D-Characters (see d_chars) */
  uint8_t copyrightid[38]; /* filename of a file in the root directory containing copyright
    information */
  uint8_t abstractfile[36]; /* filename of a file in the root directory containing abstract
    information */
  uint8_t bibliofile[37]; /* filename of a file in the root directory containing bibliographic
    information */
  iso9660_datetime_ascii_t volcreation; /* volume creation date and time */
  iso9660_datetime_ascii_t volmodification; /* volume last modification date and time */
  iso9660_datetime_ascii_t volexpiration; /* volume expiration date and time */
  iso9660_datetime_ascii_t voleffective; /* volume effective date and time (after which the volume
    should be used) */
  uint8_t filestructversion; /* always 0x01 */
  uint8_t unused3; /* set to 0 */
  uint8_t data[512]; /* application used data */
  uint8_t reserved[653]; /* reserved by ISO */
} __attribute__((packed));
typedef struct iso9660_primary_vd iso9660_primary_vd_t;

/*** Filesystem metadata ***/

#define ISO9660_PT_ENTRY_BASE_SZ 8
struct iso9660_pathtable_entry {
  uint8_t dirident_length; /* length of the directory identifier (name) */
  uint8_t extendedattrecord_length; /* extended attribute record length */
  uint32_t lba_extent; /* LBA location of the extent (note that its format is
    little-endian if in the L path table of big-endian if in the M path table) */
  uint16_t parent_index; /* index in the path table of the parent directory */
  uint8_t dirident[]; /* directory identifier (name) in d-characters */
  /* there is a last member which is present only if the length of the directory
    identifier is odd, so each entry start on an even byte number */
  // uint8_t align_to_even_byte_number;

  /* caclulating the size of a path table entry is a bit weird :
    Formula : size (bytes) = ISO9660_PT_ENTRY_BASE_SZ + entry.dirident_length +
      ((entry.dirident_length%2 == 0) ? 0 : 1)
    Indeed the size is always an even number, so we need to add 1 if
      dirident_length is odd. */
} __attribute__((packed));
typedef struct iso9660_pathtable_entry iso9660_pathtable_entry_t;

#define ISO9660_FLAG_HIDDEN 0x1
#define ISO9660_FLAG_DIRECTORY 0x2
#define ISO9660_FLAG_ASSOCIATED 0x4
#define ISO9660_FLAG_EXTATTRECORD_FORMAT 0x8

#define ISO9660_DIRRECORD_BASE_SIZE 33
struct iso9660_dirrecord {
  /* each directory record represents a file or
    a folder in the file system. Its filename is
    its identifier. The extent field is the content
    of the file (if it is a file), or a list of
    directory records representing each subitems (files
    or folders) (if it is a folder). */

  uint8_t length; /* length of the directory record (in bytes) */
  uint8_t extendedattrecord_length; /* extended attribute record length */
  uint32_lsb_msb_t lba_extent; /* LBA location of the extent */
  uint32_lsb_msb_t extent_length; /* data length (size of the extent in bytes) */
  iso9660_datetime_t creationdate; /* recording date and time */
  uint8_t flags; /* file flags :
    bit 0 : if set, file is marked as 'hidden' (user should not know the existence of the file)
    bit 1 : if set, this record describes a directory (so it is a subdirectory extent)
    bit 2 : if set, this file is an "Associated file"
    bit 3 : if set, the extended attribute record contains information about the format
      of this file
    bit 4 : if set, owner and group permissions are set in the extended attribute record
    bit 5 & 6 : reserved
    bit 7 : if set, this is not the final directory record for this file (for files spanning
      several extents, ex files over 4 GiB long) */
  uint8_t unitsize; /* file unit size if recorded in interleaved mode, zero otherwise */
  uint8_t interleavegapsize; /* interleave gap size if recorded in interleaved mode, zero otherwise */
  uint16_lsb_msb_t volsequencenb; /* number of the volume this extent is recorded on */
  uint8_t ident_length; /* length of the file identifier (file name) */
  uint8_t identifier[]; /* file identifier (terminates with a ';' followed by the file
    ID number in ASCII digits) */
  /* the following member is present only if the length of the directory
    identifier is even, so each entry start on an even byte number */
  // uint8_t align_to_even_byte_number;

  /* The remaining bytes up to the maximum record size of 255 may be used for
    extensions of ISO 9660 (Rock Ridge, ...) */
} __attribute__((packed));
typedef struct iso9660_dirrecord iso9660_dirrecord_t;

#define ISO9660_A_CHARS_LEN 57
static const char *a_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_!\"%&\'()*+,-./:;<=>? "; /* Characters allowed
  for strings other than filenames (publisher informations, ...) */
#define ISO9660_D_CHARS_LEN 37
static const char *d_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"; /* Characters allowed for identifiers
  (file names, volume identifiers, ...) (note that it does not contains lower case) */

/*** utilities ***/

static inline int iso9660_is_achars(char *s, size_t len)
{
  unsigned int i, j, isin;
  for(i = 0; i<len; i++)
  {
    isin = 0;
    for(j = 0; j<ISO9660_A_CHARS_LEN; j++)
    {
      if(a_chars[j] == s[i])
      {
        isin = 1;
        break;
      }
    }
    if(!isin) return 0;
  }
  return 1;
}

static inline int iso9660_is_dchars(char *s, size_t len)
{
  unsigned int i, j, isin;
  for(i = 0; i<len; i++)
  {
    isin = 0;
    for(j = 0; j<ISO9660_D_CHARS_LEN; j++)
    {
      if(d_chars[j] == s[i])
      {
        isin = 1;
        break;
      }
    }
    if(!isin) return 0;
  }
  return 1;
}

static inline int iso9660_isnuldate_ascii(iso9660_datetime_ascii_t *date)
{
  uint8_t *d = (uint8_t *) date;
  unsigned int i;
  for(i = 0; i<sizeof(struct iso9660_datetime_ascii)-1; i++)
  {
    if(d[i] != 0x30)
    {
      return 0;
    }
  }
  return 1;
}

static inline int iso9660_isnuldate(iso9660_datetime_t *date)
{
  uint8_t *d = (uint8_t *) date;
  unsigned int i;
  for(i = 0; i<sizeof(struct iso9660_datetime)-1; i++)
  {
    if(d[i] != 0)
    {
      return 0;
    }
  }
  return 1;
}

static inline void iso9660_print_date_ascii(FILE *out, iso9660_datetime_ascii_t *date)
{
  if(iso9660_isnuldate_ascii(date))
  {
    fprintf(out, "(null date)");
  } else
  {
    fprintf(out, "%.2s/%.2s/%.4s %.2s:%.2s:%.2s", date->day, date->month, date->year,
      date->hours, date->minutes, date->seconds);
    /* print GMT offset */
    // int gmt_offset = (15 * (date->gmt_offset - 48)) / 60;
    // fprintf(out, " GMT%s%ih", (gmt_offset>=0) ? "+" : "", gmt_offset);
  }
}

static inline void iso9660_print_date(FILE *out, iso9660_datetime_t *date)
{
  if(iso9660_isnuldate(date))
  {
    fprintf(out, "(null date)");
  } else
  {
    fprintf(out, "%2u/%2u/%4u %2u:%2u:%02u", date->day, date->month, 1900+date->year,
      date->hours, date->minutes, date->seconds);
    /* print GMT offset */
    // int gmt_offset = (15 * (date->gmt_offset - 48)) / 60;
    // fprintf(out, " GMT%s%ih", (gmt_offset>=0) ? "+" : "", gmt_offset);
  }
}

#endif
